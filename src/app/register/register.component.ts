import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterService } from '../register.service';
import { Register } from '../register.model';

@Component({
  selector: 'app-forms',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  @ViewChild('f')userForm!: NgForm;
  isUpLoading = false;
  registerUpLoadingSubscription!: Subscription;
  editedId = '';
  isEdit = false;

  constructor(private router: Router, private registerService: RegisterService, private route: ActivatedRoute){}

  ngOnInit(): void {
    this.registerUpLoadingSubscription = this.registerService.registerUpLoading.subscribe((isUpLoading: boolean) => {
      this.isUpLoading = isUpLoading;
    });

    this.route.data.subscribe(data => {
      const register = <Register | null> data.register;
      console.log(register);
      if (register){
        this.isEdit = true;
        this.editedId = register.id;
        this.setFormValue({
          name: register.name,
          lastName: register.lastName,
          surname: register.surname,
          phone: register.phone,
          gender: register.gender,
          size: register.size,
          comment: register.comment
        })
      }  else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          name: '',
          lastName: '',
          surname: '',
          phone: '',
          gender: '',
          size: '',
          comment: ''
        })
      }
    });
  }

  setFormValue(value: {[key: string]: any}){
    setTimeout(() => {
      this.userForm.form.setValue(value);
    });
  }

  onSubmit(){
    const id = this.editedId || Math.random().toString();
    const form = new Register(id, this.userForm.value.name,
      this.userForm.value.lastName, this.userForm.value.surname, this.userForm.value.phone, this.userForm.value.gender,
      this.userForm.value.size, this.userForm.value.comment);
    const next = () => {
      this.registerService.fetchRegisters();
      void this.router.navigate(['/app-register'], {relativeTo: this.route});
    };

    if (this.isEdit) {
      this.registerService.editRegister(form).subscribe(next)
    } else {
      this.registerService.getForm(form).subscribe(next);
    }
  }

  ngOnDestroy(): void {
    this.registerUpLoadingSubscription.unsubscribe();
  }

}
