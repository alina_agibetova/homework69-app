import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { RegisterFormComponent } from './register-form.component';
import { ApplicationsComponent } from './applications/applications.component';
import { RegisterResolverService } from './register/register-resolver.service';

const routes: Routes = [
  {path: '', component: RegisterComponent},
  {path: 'app-register', component: RegisterFormComponent},
  {path:':id/register', component: RegisterComponent, resolve: {register: RegisterResolverService}},
  {path: 'applications', component: ApplicationsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
