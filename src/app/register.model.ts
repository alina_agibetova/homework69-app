export class Register {
  constructor(
    public id: string,
    public name: string,
    public lastName: string,
    public surname: string,
    public phone: number,
    public gender: string,
    public size: string,
    public comment: string
  ){}
}
